package eu.specs.project.enforcement.implementation.core;

import com.github.fakemongo.Fongo;
import com.mongodb.DB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.SocketUtils;

@ContextConfiguration("classpath:implementation-core-context-test.xml")
public abstract class TestParent {
    private static final Logger logger = LogManager.getLogger(TestParent.class);

    @Autowired
    public Fongo fongo;

    @BeforeClass
    public static void setWireMockPort() {
        if (System.getProperty("wiremock.port") != null) {
            logger.debug("Property wiremock.port is already set: {}", System.getProperty("wiremock.port"));
        } else {
            int port = SocketUtils.findAvailableTcpPort();
            logger.debug("Using port {} for WireMock server.", port);
            System.setProperty("wiremock.port", Integer.toString(port));
        }
    }

    @After
    public void tearDown() throws Exception {
        for (DB db : fongo.getUsedDatabases()) {
            logger.trace("Dropping database " + db.getName());
            db.dropDatabase();
        }
    }
}
