package eu.specs.project.enforcement.implementation.core.processor.remediation;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

@Component
@Order(value = 1)
public class NumberOfServersTooLowRemediator implements IRemediator {
	private static final Logger logger = LogManager.getLogger(NumberOfServersTooLowRemediator.class);

	@Autowired
	private Provisioner provisioner;

	@Autowired
	private ImplPlanService implPlanService;

	@Override
	public boolean isApplicable(RemPlan remPlan) {
		return ( Objects.equals(remPlan.getEventId(), "number_of_servers_too_low_wp_e1") 
				|| Objects.equals(remPlan.getEventId(), "diversity_level_too_low_wp_e2"));
	}

	@Override
	public RemPlan.Result remediate(RemPlan remPlan, ImplementationPlanTwoProviders implPlan) throws ImplementationException {
		throw new ImplementationException("Unexpected number of pools.");
//		try {
//			logger.debug("NumberOfServersTooLowRemediator started, related to event "+remPlan.getEventId());
//			if (implPlan.getPools().size() != 1) {
//				throw new ImplementationException("Unexpected number of pools.");
//			}
//			ImplementationPlan.Pool pool = implPlan.getPools().get(0);
//			final String slaId = implPlan.getSlaId();
//
//			List<ImplementationPlan.Vm> webServerVms = new ArrayList<>();
//			List<ImplementationPlan.Vm> haProxyServers = new ArrayList<>();
//
//			for (ImplementationPlan.Vm vm : pool.getVms()) {
//				for (ImplementationPlan.Component component : vm.getComponents()) {
//					if (Objects.equals(component.getId(), "wp_apache") ||
//							Objects.equals(component.getId(), "wp_nginx")) {
//						webServerVms.add(vm);
//					}
//					if(Objects.equals(component.getId(), "wp_haproxy")){
//						haProxyServers.add(vm);
//					}
//				}
//			}
//
//			if (webServerVms.isEmpty()) {
//				throw new ImplementationException(String.format(
//						"No web server component found in the implementation plan %s.", implPlan.getId()));
//			}
//
//
//			// determine failed web servers
//			final List<ImplementationPlan.Vm> failedWebServerVms = new ArrayList<>();
//			for(int i=0;i<webServerVms.size();i++){
//				if(!checkIfVmisAvailable(webServerVms.get(i).getPublicIp())){
//					failedWebServerVms.add(webServerVms.get(i));
//				}
//			}
//			
//			if(failedWebServerVms.size()==0){
//				logger.debug("the number of failed VM is 0, so nothing has to be done");
//				return RemPlan.Result.OBSERVE;
//			}
//			
//			if(haProxyServers.size()==0){
//				logger.debug("the number of haproxy VM is 0, so nothing has to be done");
//				return RemPlan.Result.OBSERVE;
//			}
//
//			logger.debug("Preparing implementation plan for remediation...");
//			for (ImplementationPlan.Vm vm : failedWebServerVms) {
//				logger.debug("VM {} ({}) will be terminated and a new one will be created.",
//						vm.getVmSeqNum(), vm.getPublicIp());
//				vm.setPublicIp(null);
//				for (ImplementationPlan.Component component : vm.getComponents()) {
//					component.setPrivateIps(new ArrayList<String>());
//				}
//			}
//
//			logger.debug("Remediation implementation plan:\n{}", JsonDumper.dump(implPlan));
//
//			// delete failed web servers
//			final String providerId = implPlan.getIaas().getProvider();
//			new Thread(new Runnable() {
//				public void run() {
//					logger.debug("Calling deleteListOfVM()...");
//					provisioner.deleteListOfVM(providerId, slaId.toLowerCase(), failedWebServerVms);
//					logger.debug("deleteListOfVM() finished successfully.");
//				}
//			}).start();
//
//			// implement remediation implementation plan
//			logger.debug("Implementing remediation implementation plan...");
//			ImplementationPlan implPlanUpdated = provisioner.addNewVmsToExistingGroup(implPlan);
//			logger.debug("Remediation implementation plan implemented successfully.");
//
//			//update implementation plan on chef server
//			provisioner.updateDatabagItem(implPlanUpdated);
//			
//			
//			logger.debug("trying to execute recipe haproxy on node with ip: "+haProxyServers.get(0).getPublicIp());
//			ArrayList<String> recipes = new ArrayList<String>();
//			recipes.add("WebPool::haproxy");
//			provisioner.executeRecipes(implPlanUpdated.getIaas().getProvider(), slaId, haProxyServers, recipes);
//			
//			// store updated implementation plan to the database
//			implPlanUpdated.setCreationTime(new Date());
//			implPlanService.storeImplPlan(implPlanUpdated);
//			logger.debug("Final implementation plan (after remediation):\n{}", JsonDumper.dump(implPlanUpdated));
//
//			logger.debug("NumberOfServersTooLowRemediator finished successfully.");
//			return RemPlan.Result.OBSERVE;
//
//		} catch (Exception e) {
//			throw new ImplementationException("Failed to remediate redundancy_too_low event: " + e.getMessage(), e);
//		}
	}

	private  boolean checkIfVmisAvailable(String ipAddress){
		logger.debug("trying tocheck vm with ip {}",ipAddress);
		javax.ws.rs.client.Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target("http://"+ipAddress);

		try{
			Response response = webTarget.request().get(Response.class);

			if (response.getStatus() != 200) {
				return false;
			}
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}

	}

}
