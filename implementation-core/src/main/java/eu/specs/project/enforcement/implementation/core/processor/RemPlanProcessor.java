package eu.specs.project.enforcement.implementation.core.processor;

import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import eu.specs.project.enforcement.implementation.core.client.EventArchiverClient;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.processor.remediation.ForkAttackRemediator;
import eu.specs.project.enforcement.implementation.core.processor.remediation.IRemediator;
import eu.specs.project.enforcement.implementation.core.repository.ImplPlanRepository;
import eu.specs.project.enforcement.implementation.core.repository.RemPlanRepository;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RemPlanProcessor {
    private static final Logger logger = LogManager.getLogger(RemPlanProcessor.class);

    @Autowired
    private RemPlanRepository remPlanRepository;

    @Autowired
    private ImplPlanRepository implPlanRepository;

    @Autowired
    private AuditClient auditClient;

    @Autowired
    private ForkAttackRemediator forkAttackRemediator;

    @Autowired
    private List<IRemediator> remediators;

    public void implementRemPlan(RemPlan remPlan) {

        try {
            logger.debug("Implementing remediation plan {}...", remPlan.getId());
            long startTime = System.currentTimeMillis();

            auditClient.logComponentActivity("Implementation", CompActivity.ComponentState.ACTIVATED,
                    remPlan.getSlaId(), SlaState.REMEDIATING);

            ImplementationPlanTwoProviders implPlan = implPlanRepository.findById(remPlan.getPlanId());
            if (implPlan == null) {
                throw new ImplementationException(String.format("The implementation plan %s cannot be found.", remPlan.getPlanId()));
            }

            IRemediator remediator = null;
            for (IRemediator remediator1 : remediators) {
                if (remediator1.isApplicable(remPlan)) {
                    remediator = remediator1;
                    logger.debug("Found remediator to handle the remediation plan: {}",
                            remediator.getClass().getSimpleName());
                    break;
                }
            }

            if (remediator == null) {
                throw new ImplementationException(String.format(
                        "No remediator can be found to handle the remediation plan %s.", remPlan.getId()));
            }

            RemPlan.Result result = remediator.remediate(remPlan, implPlan);

            logger.debug("Remediation plan {} finished with result {}.", remPlan.getId(), result);

            remPlan.setResult(result);
            remPlanRepository.save(remPlan);
            logger.debug("implementRemPlan() finished successfully in {} seconds.",
                    (System.currentTimeMillis() - startTime) / 1000.0);

        } catch (Exception e) {
            String errMessage = String.format("Failed to implement remediation plan %s: %s", remPlan.getId(), e.getMessage());
            logger.error(errMessage, e);

            remPlan.setResult(RemPlan.Result.ERROR);
            remPlan.addAnnotation(new Annotation("error", errMessage, new Date()));
            remPlanRepository.save(remPlan);
        }
    }
}
