package eu.specs.project.enforcement.implementation.core.processor.remediation;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Order(value = 1)
public class ForkAttackRemediator implements IRemediator {
    private static final Logger logger = LogManager.getLogger(ForkAttackRemediator.class);

    @Autowired
    private Provisioner provisioner;

    @Autowired
    private ImplPlanService implPlanService;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public boolean isApplicable(RemPlan remPlan) {
        return Objects.equals(remPlan.getEventId(), "specs:fork_attack:DBB_E3");
    }

    @Override
    public RemPlan.Result remediate(RemPlan remPlan, ImplementationPlanTwoProviders implPlan) throws ImplementationException {
//        try {
//            if (implPlan.getPools().size() != 1) {
//                throw new ImplementationException("Unexpected number of pools.");
//            }
//            ImplementationPlan.Pool pool = implPlan.getPools().get(0);
//            final String slaId = implPlan.getSlaId();
//
//            ImplementationPlan.Vm mainE2eeServerVm = null;
//            ImplementationPlan.Component mainE2eeServerComp = null;
//            ImplementationPlan.Vm backupE2eeServerVm = null;
//            ImplementationPlan.Component backupE2eeServerComp = null;
//            ImplementationPlan.Vm masterDbVm = null;
//            ImplementationPlan.Component masterDbComp = null;
//            ImplementationPlan.Vm slaveDbVm = null;
//            ImplementationPlan.Component slaveDbComp = null;
//
//            for (ImplementationPlan.Vm vm : pool.getVms()) {
//                for (ImplementationPlan.Component component : vm.getComponents()) {
//                    if (Objects.equals(component.getId(), "DBB_main_server")) {
//                        mainE2eeServerVm = vm;
//                        mainE2eeServerComp = component;
//                    } else if (Objects.equals(component.getId(), "DBB_backup_server")) {
//                        backupE2eeServerVm = vm;
//                        backupE2eeServerComp = component;
//                    } else if (Objects.equals(component.getId(), "DBB_main_db")) {
//                        masterDbVm = vm;
//                        masterDbComp = component;
//                    } else if (Objects.equals(component.getId(), "DBB_backup_db")) {
//                        slaveDbVm = vm;
//                        slaveDbComp = component;
//                    }
//                }
//            }
//
//            if (mainE2eeServerVm == null || backupE2eeServerVm == null || masterDbVm == null || slaveDbVm == null) {
//                throw new ImplementationException(String.format(
//                        "Failed to identify VMs for the DBB components in the implementation plan %s.", implPlan.getId()));
//            }
//
//            if (mainE2eeServerComp.getPrivateIps().isEmpty() || backupE2eeServerComp.getPrivateIps().isEmpty() ||
//                    masterDbComp.getPrivateIps().isEmpty() || slaveDbComp.getPrivateIps().isEmpty()) {
//                throw new ImplementationException(String.format(
//                        "Invalid implementation plan %s. One or more IPs are not set.", implPlan.getId()));
//            }
//
//            // delete main E2EE server and master DB server
//            final List<ImplementationPlan.Vm> vmsToDelete = new ArrayList<>();
//            ImplementationPlan.Vm mainE2eeServerVmClone = objectMapper.readValue(
//                    objectMapper.writeValueAsString(mainE2eeServerVm), ImplementationPlan.Vm.class);
//            ImplementationPlan.Vm masterDbVmClone = objectMapper.readValue(
//                    objectMapper.writeValueAsString(masterDbVm), ImplementationPlan.Vm.class);
//            vmsToDelete.add(mainE2eeServerVmClone);
//            vmsToDelete.add(masterDbVmClone);
//            logger.debug("Deleting VMs {} ({}) and {} ({})...",
//                    mainE2eeServerVm.getVmSeqNum(), mainE2eeServerComp.getPrivateIps().get(0),
//                    masterDbVm.getVmSeqNum(), masterDbComp.getPrivateIps().get(0));
//            final String providerId = implPlan.getIaas().getProvider();
//            new Thread(new Runnable() {
//                public void run() {
//                    try {
//                        provisioner.deleteListOfVM(providerId, slaId.toLowerCase(), vmsToDelete);
//                        logger.debug("deleteListOfVM() finished successfully.");
//                    } catch (Exception e) {
//                        logger.error("Failed to delete compromised VMs: " + e.getMessage(), e);
//                    }
//                }
//            }).start();
//
//            logger.debug("Creating implementation plan for remediation...");
//
//            // old main E2EE server was deleted. New one will be created instead which will become backup E2EE server.
//            mainE2eeServerComp.setId("DBB_backup_server");
//            mainE2eeServerVm.setPublicIp(null);
//            for (ImplementationPlan.Component component : mainE2eeServerVm.getComponents()) {
//                component.setPrivateIps(new ArrayList<String>()); // tell Broker to create new VM
//            }
//            mainE2eeServerComp.setRecipe("E2EE::install_E2EE_server");
//            mainE2eeServerComp.setImplementationStep(1);
//
//            // old backup E2EE server becomes main E2EE server
//            backupE2eeServerComp.setId("DBB_main_server");
//
//            // old master DB server was deleted. New one will be created instead which will become slave DB server
//            masterDbComp.setId("DBB_backup_db");
//            masterDbVm.setPublicIp(null);
//            for (ImplementationPlan.Component component : masterDbVm.getComponents()) {
//                component.setPrivateIps(new ArrayList<String>()); // tell Broker to create new VM
//            }
//            masterDbComp.setRecipe("DBB::install_postgresql");
//            masterDbComp.setImplementationStep(1);
//
//            // old slave DB server becomes master DB server
//            slaveDbComp.setId("DBB_main_db");
//
//            // reorder the VMs list so that first comes new main E2EE server and new master DB VMs
//            logger.debug("Reordering VMs list...");
//            List<ImplementationPlan.Vm> vmList = pool.getVms();
//            Collections.swap(vmList, vmList.indexOf(mainE2eeServerVm), vmList.indexOf(backupE2eeServerVm));
//            Collections.swap(vmList, vmList.indexOf(masterDbVm), vmList.indexOf(slaveDbVm));
//
//            for (int i=0; i<vmList.size(); i++) {
//                vmList.get(i).setVmSeqNum(i);
//            }
//
//            logger.debug("Remediation implementation plan:\n{}", JsonDumper.dump(implPlan));
//
//            // implement remediation implementation plan
//            logger.debug("Implementing remediation implementation plan...");
//            ImplementationPlan implPlanUpdated = provisioner.addNewVmsToExistingGroup(implPlan);
//            logger.debug("Remediation implementation plan implemented successfully.");
//
//            ImplementationPlan.Vm newMasterDbVm = implPlanUpdated.getPools().get(0).getVms().get(
//                    vmList.indexOf(slaveDbVm));
//            ImplementationPlan.Vm newSlaveDbVm = implPlanUpdated.getPools().get(0).getVms().get(
//                    vmList.indexOf(masterDbVm));
//            ImplementationPlan.Vm newBackupE2eeVm = implPlanUpdated.getPools().get(0).getVms().get(
//                    vmList.indexOf(mainE2eeServerVm));
//
//            // 1. execute recipe DBB::turn_to_master on old slave DB which will become now master DB
//            List<String> newMasterDbVmPrivateIP = newMasterDbVm.getComponents().get(0).getPrivateIps();
//            List<ImplementationPlan.Vm> recipesTargetVms = new ArrayList<>();
//            recipesTargetVms.add(newMasterDbVm);
//            List<String> recipes = new ArrayList<>();
//            recipes.add("recipe[DBB::turn_to_master]");
//            logger.debug("Executing recipes {} on the VM {}, private IPs {}, SLA ID {}...",
//                    recipes, newMasterDbVm.getVmSeqNum(), newMasterDbVmPrivateIP, slaId);
//            provisioner.executeRecipes(implPlan.getIaas().getProvider(), slaId.toLowerCase(),
//                    recipesTargetVms, recipes);
//            logger.debug("executeRecipes() finished successfully.");
//            ImplementationPlan.Component newMasterDbComp = findComponent(newMasterDbVm.getComponents(), "DBB_main_db");
//            newMasterDbComp.setRecipe(newMasterDbComp.getRecipe() + "," + "DBB::turn_to_master");
//
//            // 2. execute recipe DBB::postgresql_slave on newly created slave DB VM (PostgreSQL is already installed)
//            recipesTargetVms = new ArrayList<>();
//            List<String> newSlaveDbVmPrivateIP = newSlaveDbVm.getComponents().get(0).getPrivateIps();
//            logger.debug("New slave DB: seq num {}, private IPs {}, public IP {}.",
//                    newSlaveDbVm.getVmSeqNum(), newSlaveDbVmPrivateIP, newSlaveDbVm.getPublicIp());
//            recipesTargetVms.add(newSlaveDbVm);
//            recipes = new ArrayList<>();
//            recipes.add("recipe[DBB::postgresql_slave]");
//            logger.debug("Executing recipes {} on the VM {}, private IPs {}, SLA ID {}...",
//                    recipes, newSlaveDbVm.getVmSeqNum(), newSlaveDbVmPrivateIP, slaId);
//            provisioner.executeRecipes(implPlan.getIaas().getProvider(), slaId.toLowerCase(),
//                    recipesTargetVms, recipes);
//            logger.debug("executeRecipes() finished successfully.");
//            ImplementationPlan.Component newSlaveDbComp = findComponent(newSlaveDbVm.getComponents(), "DBB_backup_db");
//            newSlaveDbComp.setRecipe(newSlaveDbComp.getRecipe() + "," + "DBB::postgresql_slave");
//
//            // 3. execute recipe E2EE::configure_and_run_E2EE on newly created backup E2EE VM (E2EE server is already installed)
//            recipesTargetVms = new ArrayList<>();
//            List<String> newBackupE2eeVmPrivateIP = newBackupE2eeVm.getComponents().get(0).getPrivateIps();
//            logger.debug("New backup E2EE: seq num {}, private IPs {}, public IP {}.",
//                    newBackupE2eeVm.getVmSeqNum(), newBackupE2eeVmPrivateIP, newBackupE2eeVm.getPublicIp());
//            recipesTargetVms.add(newBackupE2eeVm);
//            recipes = new ArrayList<>();
//            recipes.add("recipe[E2EE::configure_and_run_E2EE]");
//            logger.debug("Executing recipes {} on the VM {}, private IPs {}, SLA ID {}...",
//                    recipes, newBackupE2eeVm.getVmSeqNum(), newBackupE2eeVmPrivateIP, slaId);
//            provisioner.executeRecipes(implPlan.getIaas().getProvider(), slaId.toLowerCase(),
//                    recipesTargetVms, recipes);
//            logger.debug("executeRecipes() finished successfully.");
//            ImplementationPlan.Component newBackupE2eeComp = findComponent(newBackupE2eeVm.getComponents(), "DBB_backup_server");
//            newBackupE2eeComp.setRecipe(newBackupE2eeComp.getRecipe() + "," + "E2EE::configure_and_run_E2EE");
//
//            // store updated implementation plan to the database
//            implPlanUpdated.setCreationTime(new Date());
//            implPlanService.storeImplPlan(implPlanUpdated);
//            logger.debug("Final implementation plan (after remediation):\n{}", JsonDumper.dump(implPlanUpdated));
//
//            logger.debug("ForkAttackRemediator finished successfully.");
//            return RemPlan.Result.OBSERVE;
//
//        } catch (Exception e) {
//            throw new ImplementationException("Failed to remediate fork attack: " + e.getMessage(), e);
//        }
        
        throw new ImplementationException("Failed to remediate fork attack");
    }

//    private ImplementationPlanTwoProviders.Component findComponent(List<ImplementationPlan.Component> components, String componentId) {
//        for (ImplementationPlan.Component component : components) {
//            if (component.getId().equals(componentId)) {
//                return component;
//            }
//        }
//        return null;
//    }
}
