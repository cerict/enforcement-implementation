package eu.specs.project.enforcement.implementation.core.processor;

import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.CompActivity;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.repository.ImplActivityRepository;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class ImplActivityProcessor {
    private static final Logger logger = LogManager.getLogger(ImplActivityProcessor.class);

    @Autowired
    private ImplActivityRepository implActivityRepository;

    @Autowired
    private ImplPlanService implPlanService;

    @Autowired
    private Provisioner provisioner;

    @Autowired
    private AuditClient auditClient;

    public void implementPlan(ImplActivity implActivity, ImplementationPlanTwoProviders implPlan) {

        try {
            logger.debug("Implementing plan {}...", implPlan.getId());
            long startTime = System.currentTimeMillis();

            auditClient.logComponentActivity("Implementation", CompActivity.ComponentState.ACTIVATED,
                    implActivity.getSlaId(), SlaState.SIGNED);

            implActivity.setState(ImplActivity.Status.IMPLEMENTING);
            implActivityRepository.save(implActivity);

            ImplementationPlanTwoProviders implPlanUpdated = provisioner.deployVms(implPlan);

            // store the updated implementation plan
            implPlanService.storeImplPlan(implPlanUpdated);
            logger.debug("Updated implementation plan:\n{}", JsonDumper.dump(implPlanUpdated));

            // update the implementation activity
            implActivity.setNewPlanId(implPlanUpdated.getId());
            implActivity.setState(ImplActivity.Status.ACTIVE);
            implActivityRepository.save(implActivity);

            auditClient.logComponentActivity("Implementation", CompActivity.ComponentState.DEACTIVATED,
                    implActivity.getSlaId(), SlaState.SIGNED);

            logger.debug("Plan {} has been implemented successfully in {} seconds.", implPlan.getId(),
                    (System.currentTimeMillis() - startTime) / 1000.0);

        } catch (Exception e) {
            String errMessage = String.format("Failed to implement plan %s: %s", implPlan.getId(), e.getMessage());
            logger.error(errMessage, e);
            implActivity.setState(ImplActivity.Status.ERROR);
            implActivity.addAnnotation(new Annotation("error", errMessage, new Date()));
            implActivityRepository.save(implActivity);
        }
    }

    public void implementReconfiguredPlan(ImplActivity implActivity, ImplementationPlanTwoProviders implPlan) {

//        try {
//            logger.debug("Implementing reconfiguration plan {}...", implPlan.getId());
//            long startTime = System.currentTimeMillis();
//
//            auditClient.logComponentActivity("Implementation", CompActivity.ComponentState.ACTIVATED,
//                    implActivity.getSlaId(), SlaState.SIGNED);
//
//            implActivity.setState(ImplActivity.Status.IMPLEMENTING);
//            implActivityRepository.save(implActivity);
//
//            // terminate VMs that a marked for deletion
//            List<ImplementationPlan.Vm> vmsToRemove = new ArrayList<>();
//            Iterator<ImplementationPlan.Vm> it = implPlan.getPools().get(0).getVms().iterator();
//            while (it.hasNext()) {
//                ImplementationPlan.Vm vm = it.next();
//                if (vm.getComponents() == null) {
//                    vmsToRemove.add(vm);
//                    it.remove();
//                }
//            }
//
//            if (!vmsToRemove.isEmpty()) {
//                for (ImplementationPlan.Vm vm : vmsToRemove) {
//                    logger.debug("VM to remove: seq_num={}, public IP={}", vm.getVmSeqNum(), vm.getPublicIp());
//                }
//                provisioner.deleteListOfVM(implPlan.getIaas().getProvider(), implPlan.getSlaId(), vmsToRemove);
//            }
//
//            ImplementationPlan implPlanFinal = provisioner.addNewVmsToExistingGroup(implPlan);
//
//            // store the updated (final) implementation plan
//            implPlanService.storeImplPlan(implPlanFinal);
//            logger.debug("Final implementation plan:\n{}", JsonDumper.dump(implPlanFinal));
//
//            // update the implementation activity
//            implActivity.setNewPlanId(implPlanFinal.getId());
//            implActivity.setState(ImplActivity.Status.ACTIVE);
//            implActivityRepository.save(implActivity);
//
//            auditClient.logComponentActivity("Implementation", CompActivity.ComponentState.DEACTIVATED,
//                    implActivity.getSlaId(), SlaState.SIGNED);
//
//            logger.debug("Reconfiguration plan {} has been implemented successfully in {} seconds.", implPlan.getId(),
//                    (System.currentTimeMillis() - startTime) / 1000.0);
//
//        } catch (Exception e) {
//            String errMessage = String.format("Failed to implement reconfigured plan %s: %s",
//                    implPlan.getId(), e.getMessage());
//            logger.error(errMessage, e);
//            implActivity.setState(ImplActivity.Status.ERROR);
//            implActivity.addAnnotation(new Annotation("error", errMessage, new Date()));
//            implActivityRepository.save(implActivity);
//        }
    }
}
