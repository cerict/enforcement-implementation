package eu.specs.project.enforcement.implementation.core.service;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.broker.provisioning.Provisioner;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.processor.ImplActivityProcessor;
import eu.specs.project.enforcement.implementation.core.repository.ImplActivityRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ImplActivityService {
    private static final Logger logger = LogManager.getLogger(ImplActivityService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 3;
    private ExecutorService executorService;

    @Autowired
    private ImplActivityRepository implActivityRepository;

    @Autowired
    private ImplPlanService implPlanService;

    @Autowired
    private ImplActivityProcessor implActivityProcessor;

    @Autowired
    private Provisioner provisioner;

    public ImplActivityService() {
        executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);
    }

    public ImplActivity implementPlan(final ImplementationPlanTwoProviders implPlan) throws ImplementationException {
        logger.debug("Creating implementation activity for the implementation plan {}...", implPlan.getId());

        // store implementation plan
        implPlanService.storeImplPlan(implPlan);

        final ImplActivity implActivity = new ImplActivity();
        implActivity.setId(UUID.randomUUID().toString());
        implActivity.setCreationTime(new Date());
        implActivity.setState(ImplActivity.Status.CREATED);
        implActivity.setImplPlanId(implPlan.getId());
        implActivity.setSlaId(implPlan.getSlaId());

        implActivityRepository.save(implActivity);

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                implActivityProcessor.implementPlan(implActivity, implPlan);
            }
        });

        logger.debug("Implementation activity created successfully.");
        return implActivity;
    }

    public List<ImplActivity> findAll(Filter filter) {
        return implActivityRepository.findAll(filter);
    }

    public ImplActivity getImplActivity(String implActId) {
        return implActivityRepository.findById(implActId);
    }

    public ImplActivity implementReconfiguredPlan(final ImplActivity implActivity, final ImplementationPlanTwoProviders implPlan)
            throws ImplementationException {

        logger.debug("implementReconfiguredPlan() started for implementation activity {} and implementation plan {}.",
                implActivity.getId(), implPlan.getId());

        // store implementation plan
        implPlanService.storeImplPlan(implPlan);

        implActivity.setImplPlanId(implPlan.getId());
        implActivity.setSlaId(implPlan.getSlaId());
        implActivityRepository.save(implActivity);

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                implActivityProcessor.implementReconfiguredPlan(implActivity, implPlan);
            }
        });
        logger.debug("implementReconfiguredPlan job started.");

        logger.debug("implementReconfiguredPlan() finished successfully.");
        return implActivity;
    }

    public void deleteImplActivity(ImplActivity implActivity) throws ImplementationException {
        logger.debug("deleteImplActivity() started.");
//        try {
//        	ImplementationPlanTwoProviders implPlan = implPlanService.retrieveImplPlan(implActivity.getImplPlanId());
//
//            logger.debug("Deleting VMs... slaId:"+implActivity.getSlaId() +" - provider: "+implPlan.getIaas().getProvider() );
//            provisioner.deleteVms(implActivity.getSlaId(), implPlan.getIaas().getProvider());
//            logger.debug("VMs have been successfully deleted.");
//
//            logger.debug("Deleting implementation activity...");
//            implActivityRepository.delete(implActivity);
//
//            logger.debug("deleteImplActivity() finished successfully.");
//
//        } catch (Exception e) {
//            logger.error("deleteImplActivity() failed: failed to delete VMs: " + e.getMessage(), e);
//            throw new ImplementationException("Failed to delete VMs: " + e.getMessage(), e);
//        }
    }

    public static class Filter {
        private String slaId;
        private ImplActivity.Status status;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public ImplActivity.Status getStatus() {
            return status;
        }

        public void setStatus(ImplActivity.Status status) {
            this.status = status;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
