package eu.specs.project.enforcement.implementation.core.processor.remediation;

import eu.specs.datamodel.enforcement.*;
import eu.specs.project.enforcement.implementation.core.client.EventArchiverClient;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Order(value=10)
public class GenericRemediator implements IRemediator {
    private static final Logger logger = LogManager.getLogger(GenericRemediator.class);

    @Autowired
    private EventArchiverClient eventArchiverClient;

    @Override
    public boolean isApplicable(RemPlan remPlan) {
        return true;
    }

    @Override
    public RemPlan.Result remediate(RemPlan remPlan, ImplementationPlanTwoProviders implPlan) throws ImplementationException {
    	return null;
        // prepare metric -> SLO mapping
//        Map<String, Slo> metricSloMapping = new HashMap<>();
//        for (Slo slo : implPlan.getSlos()) {
//            metricSloMapping.put(slo.getMetricId(), slo);
//        }
//
//        RemPlan.Result result = executeRemAction(remPlan.getRemFlow(), remPlan, implPlan, metricSloMapping);
//        logger.debug("GenericRemediator finished with result {}.", result);
//        return result;
    }

    private RemPlan.Result executeRemAction(Object remFlowObj, RemPlan remPlan, ImplementationPlanTwoProviders implPlan,
                                            Map<String, Slo> metricSloMapping) throws ImplementationException {
    	return null;
//        if (remFlowObj instanceof String) {
//            String remFlowString = (String) remFlowObj;
//            logger.debug("executeRemAction('{}')", remFlowString);
//            if (Objects.equals(remFlowString, "observe")) {
//                return RemPlan.Result.OBSERVE;
//            } else if (Objects.equals(remFlowString, "notify")) {
//                return RemPlan.Result.NOTIFY;
//            } else {
//                throw new ImplementationException("Unexpected yes/no action value: " + remFlowString);
//            }
//        }
//
//        String actionId;
//        Object yesAction;
//        Object noAction;
//
//        if (remFlowObj instanceof RemFlow) {
//            RemFlow remFlow = (RemFlow) remFlowObj;
//            actionId = remFlow.getActionId();
//            yesAction = remFlow.getYesAction();
//            noAction = remFlow.getNoAction();
//        } else if (remFlowObj instanceof Map) {
//            Map remFlow = (Map) remFlowObj;
//            actionId = (String) remFlow.get("action_id");
//            yesAction = remFlow.get("yes_action");
//            noAction = remFlow.get("no_action");
//        } else {
//            throw new ImplementationException("Unexpected RemFlow object type: " + remFlowObj.getClass().getName());
//        }
//        logger.debug("executeRemAction('{}')", actionId);
//
//        if (Objects.equals(actionId, "DUMMY")) {
//            return executeRemAction(yesAction, remPlan, implPlan, metricSloMapping);
//        }
//
//        // find remediation action in the remediation plan
//        RemAction remAction = null;
//        for (RemAction remAction1 : remPlan.getRemActions()) {
//            if (remAction1.getName().equals(actionId)) {
//                remAction = remAction1;
//                break;
//            }
//        }
//        if (remAction == null) {
//            throw new ImplementationException(String.format(
//                    "The remediation action %s cannot be found in the remediation plan.", actionId));
//        }
//        logger.debug("Remediation action: {}", remAction.getName());
//
//        // find Chef recipes
//        logger.debug("Chef recipes to execute: {}", remAction.getRecipes());
//        List<ChefRecipe> chefRecipes = new ArrayList<>();
//        for (ChefRecipe chefRecipe : remPlan.getChefRecipes()) {
//            if (remAction.getRecipes().contains(chefRecipe.getName())) {
//                chefRecipes.add(chefRecipe);
//            }
//        }
//        if (chefRecipes.size() != remAction.getRecipes().size()) {
//            throw new ImplementationException(String.format(
//                    "One or more Chef recipes of %s cannot be found in the remediation plan.", remPlan.getChefRecipes()));
//        }
//
//        Set<String> dependentComponents = new HashSet<>();
//        for (ChefRecipe chefRecipe : chefRecipes) {
//            dependentComponents.addAll(chefRecipe.getDependentComponents());
//        }
//        logger.debug("Dependent components: {}", dependentComponents);
//
//        // determine the VMs on which to execute the remediation action
//        List<ImplementationPlan.Vm> vms = new ArrayList<>();
//        String cookbook = null;
//        for (ImplementationPlan.Pool pool : implPlan.getPools()) {
//            for (ImplementationPlan.Vm vm : pool.getVms()) {
//                for (ImplementationPlan.Component component : vm.getComponents()) {
//                    if (dependentComponents.contains(component.getId())) {
//                        cookbook = component.getCookbook();
//                        vms.add(vm);
//                        break;
//                    }
//                }
//            }
//        }
//
//        // prepare list of recipes
//        List<String> recipes = new ArrayList<>();
//        for (String recipeName : remAction.getRecipes()) {
//            recipes.add(cookbook + "::" + recipeName);
//        }
//
//        if (logger.isDebugEnabled()) {
//            List<String> ipList = new ArrayList<>();
//            for (ImplementationPlan.Vm vm : vms) {
//                ipList.add(vm.getPublicIp());
//            }
//            logger.debug("Executing recipes {} on VMs {}...", recipes, ipList);
//        }
//
//        // find measurement corresponding to remPlan.getEventId()
//        Measurement measurement = null;
//        for (Measurement measurement1 : implPlan.getMeasurements()) {
//            if (measurement1.getMonitoringEvent().getId().equals(remPlan.getEventId())) {
//                measurement = measurement1;
//                break;
//            }
//        }
//        if (measurement == null) {
//            throw new ImplementationException(String.format(
//                    "Measurement corresponding to event id %s cannot be found in the implementation plan %s.",
//                    remPlan.getEventId(), implPlan.getId()));
//        }
//
//        Date recipesStartTime = new Date();
//        // TODO: call Broker: executeRecipes(List<ImplementationPlan.Vm> vms, List<String> recipes)
//        logger.debug("Recipes executed successfully.");
//
//        // query Event Archiver for a new monitoring event
//        logger.debug("Retrieving new metric value in order to check if it is still violated...");
//        MonitoringEvent monitoringEvent = eventArchiverClient.getLatestMonitoringEvent(
//                remPlan.getSlaId(), measurement, recipesStartTime);
//
//        // check if monitoring event is still in violation
//        boolean isViolated = evaluateCondition(measurement, monitoringEvent.getData(), metricSloMapping);
//
//        if (isViolated) {
//            logger.debug("Calling no_action...");
//            return executeRemAction(noAction, remPlan, implPlan, metricSloMapping);
//        } else {
//            logger.debug("Calling yes_action...");
//            return executeRemAction(yesAction, remPlan, implPlan, metricSloMapping);
//        }
    }

    private boolean evaluateCondition(Measurement measurement, String valueString,
                                      Map<String, Slo> metricSloMapping) throws ImplementationException {

        try {
            Measurement.MonitoringEvent.Condition condition = measurement.getMonitoringEvent().getCondition();
            String thresholdValue;
            if (condition.getThreshold().startsWith("metric:")) {
                String metric = condition.getThreshold().substring("metric:".length());
                thresholdValue = metricSloMapping.get(metric).getValue();
            } else {
                thresholdValue = condition.getThreshold();
            }

            // TODO: how to determine measurement data type? Should it be specified in the mechanism?
            DataType dataType;
            if ("true".equals(thresholdValue) || "false".equals(thresholdValue)) {
                dataType = DataType.BOOLEAN;
            } else if (thresholdValue.matches("^-?\\d+(.\\d+)?$")) {
                dataType = DataType.DOUBLE;
            } else if (thresholdValue.matches("^-?\\d+$")) {
                dataType = DataType.INTEGER;
            } else {
                throw new ImplementationException(String.format("Unable to determine data type of the measurement %s.",
                        measurement.getId()));
            }

            logger.debug("Evaluating {} condition {} {} {}...", dataType, valueString, condition.getOperator(), thresholdValue);

            if (dataType == DataType.INTEGER) {
                int threshold = Integer.parseInt(thresholdValue);
                int value = Integer.parseInt(valueString);
                switch (condition.getOperator()) {
                    case "eq":
                        return value == threshold;
                    case "neq":
                        return value != threshold;
                    case "gt":
                        return value > threshold;
                    case "geq":
                        return value >= threshold;
                    case "lt":
                        return value < threshold;
                    case "leq":
                        return value <= threshold;
                    default:
                        throw new ImplementationException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else if (dataType == DataType.DOUBLE) {
                double threshold = Double.parseDouble(thresholdValue);
                double value = Double.parseDouble(valueString);
                switch (condition.getOperator()) {
                    case "gt":
                        return value > threshold;
                    case "geq":
                        return value >= threshold;
                    case "lt":
                        return value < threshold;
                    case "leq":
                        return value <= threshold;
                    default:
                        throw new ImplementationException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else if (dataType == DataType.BOOLEAN) {
                switch (condition.getOperator()) {
                    case "eq":
                        return valueString.equals(thresholdValue);
                    case "neq":
                        return !valueString.equals(thresholdValue);
                    default:
                        throw new ImplementationException(String.format(
                                "Invalid operator for data type %s: %s", dataType, condition.getOperator()));
                }
            } else {
                throw new ImplementationException("Invalid data type:" + dataType);
            }
        } catch (Exception e) {
            throw new ImplementationException(String.format("Failed to evaluate measurement %s condition: %s",
                    measurement.getId(), e.getMessage()), e);
        }
    }

    private enum DataType {
        INTEGER,
        DOUBLE,
        BOOLEAN
    }
}
