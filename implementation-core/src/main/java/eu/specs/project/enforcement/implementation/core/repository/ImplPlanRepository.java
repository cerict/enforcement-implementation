package eu.specs.project.enforcement.implementation.core.repository;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImplPlanRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public List<ImplementationPlanTwoProviders> getAll(String slaId) {
        Query query = new Query();
        if (slaId != null) {
            query.addCriteria(Criteria.where("slaId").is(slaId));
        }

        query.fields().include("id");
        return mongoTemplate.find(query, ImplementationPlanTwoProviders.class);
    }

    public ImplementationPlanTwoProviders findById(String id) {
        return mongoTemplate.findById(id, ImplementationPlanTwoProviders.class);
    }

    public void save(ImplementationPlanTwoProviders implPlan) {
        mongoTemplate.save(implPlan);
    }

    public void delete(ImplementationPlanTwoProviders implPlan) {
        mongoTemplate.remove(implPlan);
    }
}
