package eu.specs.project.enforcement.implementation.core.service;

import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.repository.ImplPlanRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImplPlanService {
    private static final Logger logger = LogManager.getLogger(ImplPlanService.class);

    @Autowired
    private ImplPlanRepository implPlanRepository;

    public List<ImplementationPlanTwoProviders> getAll(String slaId) {
        return implPlanRepository.getAll(slaId);
    }

    public void storeImplPlan(ImplementationPlanTwoProviders implPlan) throws ImplementationException {
        logger.debug("Storing implementation plan {}...", implPlan.getId());
        implPlanRepository.save(implPlan);
        logger.debug("Implementation plan has been stored succesfully.");
    }

    public ImplementationPlanTwoProviders retrieveImplPlan(String implPlanId) {
        logger.debug("Retrieving implementation plan {}...", implPlanId);
        return implPlanRepository.findById(implPlanId);
    }
}
