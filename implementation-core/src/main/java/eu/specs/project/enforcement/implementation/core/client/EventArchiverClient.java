package eu.specs.project.enforcement.implementation.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import eu.specs.datamodel.enforcement.Measurement;
import eu.specs.datamodel.enforcement.MonitoringEvent;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.util.AppConfig;
import eu.specs.project.enforcement.implementation.core.util.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

@Component
public class EventArchiverClient {
    private static final Logger logger = LogManager.getLogger(EventArchiverClient.class);
    private static final String MON_EVENTS_PATH = "/monitoring/events";
    private WebTarget eventArchiverTarget;
    private AppConfig appConfig;

    @Autowired
    public EventArchiverClient(AppConfig appConfig) {
        this.appConfig = appConfig;
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        eventArchiverTarget = client.target(appConfig.getEventArchiverAddress());
    }

    public MonitoringEvent getLatestMonitoringEvent(String slaId, Measurement measurement, Date fromTime)
            throws ImplementationException {
        try {
            logger.debug("Retrieving new monitoring event from the Event Archiver for measurement {}, sla ID {} " +
                    "with timestamp after {}...", measurement.getId(), slaId, fromTime);

            Date startTime = new Date();

            do {
                JsonObject filter = new JsonObject();
                JsonObject timestampCond = new JsonObject();
                timestampCond.add("$gt", new JsonPrimitive(fromTime.getTime()));
                filter.add("timestamp", timestampCond);
                logger.debug("Using filter: {}", filter.toString());
                List<MonitoringEvent> monitoringEvents = eventArchiverTarget
                        .path(MON_EVENTS_PATH)
                        .queryParam("filter", "")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get(new GenericType<List<MonitoringEvent>>() {
                        });

                if (monitoringEvents.isEmpty()) {
                    logger.trace("No monitoring events returned. Retrying");
                    Thread.sleep((long) appConfig.getRemediationEventQueryInterval() * 1000);
                    continue;
                }

                MonitoringEvent monitoringEvent = monitoringEvents.get(0);

                if (monitoringEvent.getTimestamp().getTime() < fromTime.getTime()) {
                    logger.trace("Monitoring event is obsolete. Retrying...");
                    Thread.sleep((long) appConfig.getRemediationEventQueryInterval() * 1000);
                    continue;
                }

                if (!monitoringEvent.getSlaId().equals(slaId) ||
                        !monitoringEvent.getMetricId().equals(measurement.getId())) {
                    throw new ImplementationException("Invalid monitoring event received from the Event Archiver.");
                }

                logger.debug("Monitoring event retrieved successfully.");
                if (logger.isTraceEnabled()) {
                    logger.trace("Monitoring event:\n{}", JsonDumper.dump(monitoringEvent));
                }
                return monitoringEvent;

            } while (new Date().getTime() - startTime.getTime() < appConfig.getRemediationEventTimeout() * 1000);

            throw new ImplementationException("Timeout waiting for the remediation event from the Event Archiver.");

        } catch (Exception e) {
            throw new ImplementationException("Failed to query Event Archiver: " + e.getMessage(), e);
        }
    }
}
