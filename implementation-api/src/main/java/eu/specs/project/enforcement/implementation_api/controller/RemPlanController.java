package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.service.RemPlanService;
import eu.specs.project.enforcement.implementation_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value = "/rem-plans")
public class RemPlanController {

    @Autowired
    private RemPlanService remPlanService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<Map> implementRemPlan(@RequestBody RemPlan remPlan, HttpServletRequest request) {

        String remPlanId = remPlanService.implementRemPlan(remPlan);

        URI location = URI.create(request.getRequestURL().append("/").append(remPlan.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);

        Map result = new HashMap();
        result.put("rem_plan_id", remPlanId);

        return new ResponseEntity<>(result, headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{remPlanId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public RemPlan getRemPlan(@PathVariable("remPlanId") String remPlanId) {
        RemPlan remPlan = remPlanService.getRemPlan(remPlanId);
        if (remPlan == null) {
            throw new ResourceNotFoundException(String.format(
                    "The remediation plan %s cannot be found.", remPlanId));
        } else {
            return remPlan;
        }
    }

    @RequestMapping(value = "/{remPlanId}/result", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Map getRemPlanResult(@PathVariable("remPlanId") String remPlanId) {
        RemPlan remPlan = remPlanService.getRemPlan(remPlanId);
        if (remPlan == null) {
            throw new ResourceNotFoundException(String.format(
                    "The remediation plan %s cannot be found.", remPlanId));
        } else {
            Map<String, Object> result = new HashMap<>();
            result.put("result", remPlan.getResult());
            return result;
        }
    }
}
