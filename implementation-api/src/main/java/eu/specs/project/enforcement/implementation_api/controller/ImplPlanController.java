package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.service.ImplPlanService;
import eu.specs.project.enforcement.implementation_api.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "/impl-plans")
public class ImplPlanController {

    @Autowired
    private ImplPlanService implPlanService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity storeImplPlan(@RequestBody ImplementationPlanTwoProviders implPlan, HttpServletRequest request) throws ImplementationException {
        implPlanService.storeImplPlan(implPlan);

        URI location = URI.create(request.getRequestURL().append("/").append(implPlan.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getImplementationPlans(
            @RequestParam(value = "slaId", required = false) String slaId,
            HttpServletRequest request) {
        List<ImplementationPlanTwoProviders> implPlans = implPlanService.getAll(slaId);
        ResourceCollection collection = new ResourceCollection();
        collection.setResource("impl-plans");
        collection.setMembers(implPlans.size());
        collection.setTotal(implPlans.size());

        for (ImplementationPlanTwoProviders implPlan : implPlans) {
            String uri = request.getRequestURL() + "/" + implPlan.getId();
            collection.addItem(new ResourceCollection.Item(implPlan.getId(), uri));
        }
        return collection;
    }

    @RequestMapping(value = "/{implPlanId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ImplementationPlanTwoProviders getImplementationPlan(@PathVariable("implPlanId") String implPlanId) {
    	ImplementationPlanTwoProviders implPlan = implPlanService.retrieveImplPlan(implPlanId);
        if (implPlan == null) {
            throw new ResourceNotFoundException(String.format(
                    "The implementation plan %s cannot be found.", implPlanId));
        } else {
            return implPlan;
        }
    }
}
