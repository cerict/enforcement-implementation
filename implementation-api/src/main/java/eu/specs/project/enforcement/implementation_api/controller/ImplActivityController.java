package eu.specs.project.enforcement.implementation_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlanTwoProviders;
import eu.specs.project.enforcement.implementation.core.exception.ImplementationException;
import eu.specs.project.enforcement.implementation.core.service.ImplActivityService;
import eu.specs.project.enforcement.implementation_api.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping(value = "/impl-activities")
public class ImplActivityController {
    private static final String IMPL_ACTIVITY_PATH = "/impl-activities/{id}";

    @Autowired
    private ImplActivityService implActivityService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<ImplActivity> createImplActivity(@RequestBody ImplementationPlanTwoProviders implPlan,
                                                           HttpServletRequest request) throws ImplementationException {

        ImplActivity implActivity = implActivityService.implementPlan(implPlan);

        URI location = URI.create(request.getRequestURL().append("/").append(implActivity.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(implActivity, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getAll(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "status", required = false) ImplActivity.Status status,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit) {
        ImplActivityService.Filter filter = new ImplActivityService.Filter();
        filter.setSlaId(slaId);
        filter.setStatus(status);
        filter.setOffset(offset);
        filter.setLimit(limit);
        List<ImplActivity> implActivities = implActivityService.findAll(filter);
        ResourceCollection collection = new ResourceCollection();
        for (ImplActivity implActivity : implActivities) {
            URI uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .pathSegment(IMPL_ACTIVITY_PATH)
                    .buildAndExpand(implActivity.getId())
                    .toUri();
            collection.addItem(new ResourceCollection.Item(implActivity.getId(), uri.toString()));
        }
        collection.setTotal(collection.getItemList().size());
        collection.setMembers(collection.getItemList().size());
        return collection;
    }

    @RequestMapping(value = "/{implActId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ImplActivity getImplActivity(@PathVariable("implActId") String implActId) {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException(String.format(
                    "The implementation activity %s cannot be found.", implActId));
        } else {
            return implActivity;
        }
    }

    @RequestMapping(value = "/{implActId}", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void reconfigureImplActivity(@PathVariable("implActId") String implActId,
                                        @RequestBody ImplementationPlanTwoProviders implPlan) throws ImplementationException {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException(String.format(
                    "The implementation activity %s cannot be found.", implActId));
        } else {
            implActivityService.implementReconfiguredPlan(implActivity, implPlan);
        }
    }

    @RequestMapping(value = "/{implActId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteImplActivity(@PathVariable("implActId") String implActId) throws ImplementationException {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException(String.format(
                    "The implementation activity %s cannot be found.", implActId));
        } else {
            implActivityService.deleteImplActivity(implActivity);
        }
    }

    @RequestMapping(value = "/{implActId}/status", method = RequestMethod.GET, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public String getImplActivityStatus(@PathVariable("implActId") String implActId) {
        ImplActivity implActivity = implActivityService.getImplActivity(implActId);
        if (implActivity == null) {
            throw new ResourceNotFoundException(String.format(
                    "The implementation activity %s cannot be found.", implActId));
        } else {
            return implActivity.getState().name();
        }
    }
}
