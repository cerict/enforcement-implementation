package eu.specs.project.enforcement.implementation_api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.RemPlan;
import eu.specs.project.enforcement.implementation.core.repository.ImplPlanRepository;
import eu.specs.project.enforcement.implementation_api.mock.AuditingMock;
import eu.specs.project.enforcement.implementation_api.mock.EventArchiverMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.Map;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:implementation-api-context-test.xml")
public class RemPlanControllerTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ImplementationApiTest.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ImplPlanRepository implPlanRepository;

    @Autowired
    private AuditingMock auditingMock;

    @Autowired
    private EventArchiverMock eventArchiverMock;

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(
            wireMockConfig()
                    .port(Integer.parseInt(System.getProperty("wiremock.port")))
                    .extensions(new EventArchiverMock.EventArchiverMockTransformer()));

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        auditingMock.init();
        eventArchiverMock.init();
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void test() throws Exception {
        ImplementationPlan implPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/impl-plan-rem.json"), ImplementationPlan.class);

        implPlanRepository.save(implPlan);

        RemPlan remPlan = objectMapper.readValue(
                this.getClass().getResourceAsStream("/rem-plan.json"), RemPlan.class);

        MvcResult result = mockMvc.perform(post("/rem-plans")
                .content(objectMapper.writeValueAsString(remPlan)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        Map<String, String> resultMap = objectMapper.readValue(
                result.getResponse().getContentAsString(), new TypeReference<Map<String, String>>() {
                });

        String remPlanId = resultMap.get("rem_plan_id");
        assertNotNull(remPlanId);

        Date startTime = new Date();
        RemPlan.Result remPlanResult;
        while (true) {
            result = mockMvc.perform(get("/rem-plans/{0}/result", remPlanId))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andReturn();
            resultMap = objectMapper.readValue(
                    result.getResponse().getContentAsString(), new TypeReference<Map<String, String>>() {
                    });
            if (resultMap.get("result") != null) {
                remPlanResult = RemPlan.Result.valueOf(resultMap.get("result"));
                break;
            }
            if (new Date().getTime() - startTime.getTime() > 10000) {
                fail("Timeout waiting for the remediation plan to finish.");
            }
            Thread.sleep(500);
        }

        assertEquals(remPlanResult, RemPlan.Result.OBSERVE);

        mockMvc.perform(get("/rem-plans/{0}", remPlanId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.rem_plan_id", is(remPlanId)))
                .andExpect(jsonPath("$.result", is(RemPlan.Result.OBSERVE.name())));
    }
}