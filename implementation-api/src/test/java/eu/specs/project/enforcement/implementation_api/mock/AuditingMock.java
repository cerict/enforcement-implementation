package eu.specs.project.enforcement.implementation_api.mock;


import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class AuditingMock {

    public void init() throws IOException {
        stubFor(post(urlPathEqualTo("/sla-auditing/comp-activities"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(201)));
    }
}
